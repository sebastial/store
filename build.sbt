name := "store"

version := "0.1"

scalaVersion := "2.12.8"

Compile/mainClass := Some("es.saguila.examples.scala.shop.App")

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

# Store - Scala Challenge
Store es un algoritmo que usa como concepto una tienda para resolver un reto concreto de programación (actuar sobre un conjunto discreto de entradas y generar la salida que se espera),en ningun caso es una plantilla para la gestión de la logica de negocio de una tienda.Para este propósito seria necesario reestructurar y generalizar los componentes que intervienen en la lógica e implementar una arquitectura usando patrones de diseño software con el fin de reducir el acoplamiento,mejorar la reusabilidad, el aislamiento y la cohesion.
El alcance del desarrollo es dar solucion al problema planteado usando programacion funcional sin centrarse en la arquitectura.
Implementar una arquitectura correcta que permita tener un solucion robusta, flexible y adaptable al cambio es una labor que requiere tiempo y estudio.

## Features
  - Desarrollado integramente con programación funcional.
  - Scala version 2.12.8
  - Construido usando sbt

## Installation
#### Con el terminal apuntando a la ruta raiz del proyecto
```sh
sbt package
scala ./target/scala-2.12/shop_2.12-0.1.jar
```

## Future improvements
Con el fin de convertir este reto de programación en un posible algoritmo de gestión de la logica de negocio de una tienda seria interesante centrarse en los siguiente puntos:
   - Generalizar la clase Product para que puedan gestionar los productos de forma flexible y dinámica.
   - Generalizar la clase Discounts o darle una vuelta por si es interesante incluirla dentro de la clase Product.Pero sobre todo centrar los esfuerzos en crear una lógica para la aplicación de los descuentos,seria interesante generar una sintaxis de promociones para realizar los descuentos devolviendo una operacion y usar con la clase Dicounts en una variable high order functions para resolver el descuento,si fuera un caso real no tendria sentido cada vez que se lance una promoción tener que modificar el codigo,este es un punto crítico.
   - Usar patrones de diseño para hacer una aplicación mas robusta y adaptable al cambio,ej usar Factorias en caso de que existan varias fuentes para los Daos usar AbstractsFactory.
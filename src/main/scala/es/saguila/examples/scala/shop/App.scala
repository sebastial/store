package es.saguila.examples.scala.shop


/**
  * Entry point of the App.
  * @author saguila
  */
object App {
  def main(args: Array[String]): Unit = {
    /* condition added to call from Tests for increase test coverage code */
    val inputStr = if (args.isEmpty) scala.io.StdIn.readLine() else args.mkString(" ")
    val Store = new Store(inputStr)
    println(Store.buy())
  }
}

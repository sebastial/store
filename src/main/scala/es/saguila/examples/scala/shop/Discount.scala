package es.saguila.examples.scala.shop

/**
  * @author saguila
  * Create a Discount.
  * BigDecimal explanation: for currency is better use BigDecimal instead Double.
  * @param description discount explanation.
  * @param amount total amount discounted.
  * For currency better use BigDecimal instead Double.
  */

//TODO: Define a field that contains expresion to analize for do the discounts,using a kind of sintax.

case class Discount(description: String, amount: BigDecimal){
  override def toString: String = "%s: -%s€".format(this.description,this.amount)
}


package es.saguila.examples.scala.shop

/**
  * Create a Product.
  * BigDecimal explanation: for currency is better use BigDecimal instead Double.
  * @param name name of product.
  * @param price price for unity of product.
  *
  */
case class Product(name: String,price : BigDecimal){
  override def toString: String = this.name
}

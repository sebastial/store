package es.saguila.examples.scala.shop

import es.saguila.examples.scala.shop.Store.inputRegExpresion
import es.saguila.examples.scala.shop.exception.{InputValidationException, ProductParseException}

/**
  * @author saguila
  * This is a Store concept using scala for solve the given problem only, never be used as template for a store logic layer,
  * for that propose needs improve reusability, cohesion and decrease coupling,redistributing the code, defining a better architecture using
  * design patterns. Define a good architecture require time.
  */


object Store {

  /* The best option for validate input String is use a regex */
  val inputRegExpresion = "^%s( [a-zA-Z]*)*$"

  /* BigDecimal explanation: for currency is better use BigDecimal instead Double. */
  val exchangeRateEurLibra : BigDecimal = BigDecimal("0.9")
  val exchangeRateLibraEur : BigDecimal = (BigDecimal("1") / exchangeRateEurLibra).setScale(2,BigDecimal.RoundingMode.HALF_UP)


  /* All kinds of products,the best solution for prices is convert in static form the price in the same currency,
  If an exchange is necessary, do later with total amount */

  val Soup = new Product("Soup", BigDecimal("0.65") * exchangeRateLibraEur)
  val Bread = new Product("Bread", BigDecimal("0.8") * exchangeRateLibraEur)
  val Milk = new Product("Milk", BigDecimal("1.3") * exchangeRateLibraEur)
  val Apples = new Product("Apples", BigDecimal("1") * exchangeRateLibraEur)
  val Peanuts = new Product("Peanuts", BigDecimal("2"))
  val Banana = new Product("Banana", BigDecimal("1.2"))

}


  /***
    * Store class
    *
    *
    */
  class Store(inputStr :String) {

    val shoppingCart = getShoppingCartFromInput(inputStr)

    val promos = getDiscounts(shoppingCart)


    /***
      *
      * @param inputStr
      * @return
      */
    private def getShoppingCartFromInput(inputStr : String) : scala.collection.immutable.HashMap[Product, Long] = {
      val inputValidate = validateInput(inputStr)
      getShoppingCart(inputValidate.split(" ").map(product => parseProduct(product)).toList)
    }

    /***
      *
      * @return
      */
    def buy(): String = {
          val totalAmount = totalAmountCart(shoppingCart)
          payment(totalAmount,promos)
    }

    /** *
      * Validate the input String
      *
      * @param input
      * @param operation
      * @return input without part of operation
      */
    private def validateInput(input: String, operation: String = "PriceBasket"): String = {

      val regex: scala.util.matching.Regex = inputRegExpresion.format(operation).r
      regex.findFirstMatchIn(input) match {
        case Some(_) => input.substring(operation.length + 1) //Skipping Operation Name From input.
        case None => throw new InputValidationException(input)
      }
    }


    /**
      * Gets a list with the discounts that exist in shopping cart.
      * @param shoppingCart shopping cart represented as Map struct with product as Key and quantity of product as .
      * @return List with all discounts in shopping cart.
      */
    private def getDiscounts(shoppingCart : Map[Product,Long]) : List[Discount] =  {
      shoppingCart.foldRight(List[Discount]()){(kv,acc) =>
        kv._1 match {
          case Store.Apples => {
            val amount : BigDecimal = (BigDecimal(0.1) * (kv._2 * kv._1.price)).setScale(2,BigDecimal.RoundingMode.HALF_UP)
            new Discount("Apples 10% off",amount) :: acc
          }
          case Store.Bread => {
            //TODO:Repair.
            val amount : BigDecimal = scala.math.min(shoppingCart.getOrElse(Store.Soup,0L)/ 2L,kv._2) * BigDecimal("0.5") * kv._1.price
            if (amount.intValue() == 0)
              acc
            else
              new Discount("Buy 2 tins of soup and get a loaf 50% off", amount.setScale(2,BigDecimal.RoundingMode.HALF_UP))  :: acc

          }
          case _ => acc
        }
      }
    }

    /**
      * Get minimum between two BigDecimals. Note: scala.math.min not support BigDecimal.
      * @param a BigDecimal
      * @param b BigDecimal
      * @return minimum between 'a' and 'b' numbers.
      */
    private def min(a : BigDecimal,b :BigDecimal) : BigDecimal = if(a <= b) a else b

    private def totalAmountCart(shoppingCart : Map[Product,Long]) : BigDecimal = shoppingCart.foldRight(BigDecimal("0"))((cartElem,acc) => acc + cartElem._1.price * cartElem._2).setScale(2,BigDecimal.RoundingMode.HALF_UP)


    private def payment(amount : BigDecimal, discounts : List[Discount]) : String = {
     if(discounts.isEmpty)
       "Subtotal: %s€ (No offers available)\nTotal price: %s€\n".format(amount,amount)
      else{
       val subtotalStr = "Subtotal: %s€\n".format(amount)
       val (finalPrice,discStr) = discounts.foldRight((amount,"")){
         (discount,acc) => {
           (acc._1 - discount.amount,acc._2 ++ (discount.toString + '\n'))
         }
       }
       subtotalStr ++ discStr ++ "Total price: %s€".format(finalPrice.setScale(2,BigDecimal.RoundingMode.HALF_UP))
     }
    }

    /**
      * Get the shopping cart from list of products.
      * @param items List with products parsed from input.
      * @return shoppingCart with products from input in HashMap struct.
      */
    private def getShoppingCart(items: List[Product]): scala.collection.immutable.HashMap[Product, Long] = {
      items.foldRight(scala.collection.immutable.HashMap.empty[Product,Long])((item,map) => map.updated(item,map.getOrElse(item,0L) + 1L))
    }

    /** *
      * Create a product parsing the input
      *
      * @param str input str
      * @return Product that matchs with input.
      */
   private def parseProduct(str: String): Product = {
      str match {
        case "Soup" => Store.Soup
        case "Bread" => Store.Bread
        case "Milk" => Store.Milk
        case "Apples" => Store.Apples
        case "Peanuts" => Store.Peanuts
        case "Banana" => Store.Banana
        case _ => throw new ProductParseException(str)
      }
    }

  }

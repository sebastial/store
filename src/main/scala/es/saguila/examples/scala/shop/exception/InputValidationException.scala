package es.saguila.examples.scala.shop.exception

/**
  * @author saguila
  * Exception caused by invalid input format.
  * @param inputStr
  */
class InputValidationException(inputStr : String) extends Exception("Input: '%s' is invalid must be in format [PriceBasket item1 item2 item3 ...].".format(inputStr))

package es.saguila.examples.scala.shop.exception

/**
  * @author saguila
  * Exception caused by invalid product in parse process.
  * @param productFromInput
  */
class ProductParseException(productFromInput : String) extends Exception("Product from input: '%s' is invalid.".format(productFromInput))

package es.saguila.examples.scala.shop

import es.saguila.examples.scala.shop.exception.{InputValidationException, ProductParseException}
import org.scalatest.FunSuite


/***
  * Class for testing all components in App.
  * @author saguila
  */
class StoreTest extends FunSuite {


  test("Bad format of input should throws InputValidationException") {

    val badInput = "badInput"
    val caught = intercept[InputValidationException]{
      val storeInstance = new Store(badInput)
    }

    val isExpectedExc = caught match {
      case ex : InputValidationException => true
      case _ => false
    }

    assert(isExpectedExc && caught.getMessage.equals("Input: '%s' is invalid must be in format [PriceBasket item1 item2 item3 ...].".format(badInput)))
  }

  test("Invalid product from input should throws ProductParseException") {

    val badInput = "PriceBasket Apples BadProduct"

    val caught = intercept[ProductParseException]{
      val storeInstance = new Store(badInput).buy()
    }

    val isExpectedExc = caught match {
      case ex : ProductParseException => true
      case _ => false
    }

    assert(isExpectedExc && caught.getMessage.equals("Product from input: 'BadProduct' is invalid.".format(badInput)))
  }

  test("Invalid product from input going to App.main for increase test coverage should ProductParseException") {

    val badInput = Array("PriceBasket","Apples","BadProduct")


    val caught = intercept[ProductParseException]{
      App.main(badInput)
    }

    val isExpectedExc = caught match {
      case ex : ProductParseException => true
      case _ => false
    }

    assert(isExpectedExc && caught.getMessage.equals("Product from input: 'BadProduct' is invalid.".format(badInput)))
  }

 test("Complete flow should match with expected output with promotion Apples") {

    val input = "PriceBasket Apples Peanuts Apples"
    val output = new Store(input).buy()
    val outputExpected = "Subtotal: 4.22€\nApples 10% off: -0.22€\nTotal price: 4.00€"

    assert(output.equals(outputExpected))
  }
}
